package jenia.taskmanager.storages;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import jenia.taskmanager.ConnectionDetector;
import jenia.taskmanager.MainActivity;
import jenia.taskmanager.interfaces.RemoteStorage;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by jenia on 17.05.18.
 */

public class API implements RemoteStorage {
    private String serverURL = "http://192.168.1.3/api/";
    private ConnectionDetector connection;
    private OkHttpClient client = new OkHttpClient();

    public API(Context context){
        connection = new ConnectionDetector(context);
    }

    @Override
    public boolean hasConnection() {
        return connection.ConnectingToInternet();
    }

    @Override
    public String postRequest(String url, HashMap<String,String> data) {

        MultipartBody.Builder sendData = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for(String s:data.keySet()){
            sendData.addFormDataPart(s,data.get(s));
        }
        RequestBody requestBody = sendData.build();

        Request request = new Request.Builder()
                .url(serverURL+url)
                .post(requestBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            String r = response.body().string();
            Log.e("REPSONSE_____",r);
            return r;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String getRequest(String url) {

        Request request = new Request.Builder()
                .url(serverURL+url)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String postRequestLogged(String url, HashMap<String,String> data) {
        data.put("user_id",MainActivity.storage.get("user_id"));
        Log.e("data",data.toString());
        return postRequest(url,data);
    }

    public JSONObject register(String email, String password, String name) throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        data.put("email",email);
        data.put("password",password);
        data.put("name",name);
        JSONObject result = new JSONObject(postRequest("register",data));
        return result;
    }

    public JSONObject login(String email, String password) throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        data.put("email",email);
        data.put("password",password);
        JSONObject result = new JSONObject(postRequest("login",data));
        return result;
    }

    public JSONObject next_tasks() throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        JSONObject result = new JSONObject(postRequestLogged("tasks",data));
        return result;
    }

    public JSONObject compleated_tasks() throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        String resp = postRequestLogged("completed",data);
        JSONObject result = new JSONObject(resp);
        return result;
    }

    public JSONObject all_tasks() throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        String resp = postRequestLogged("all_tasks",data);
        JSONObject result = new JSONObject(resp);
        return result;
    }

    public void compleate_task(String id) throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        data.put("id",id);
        JSONObject result = new JSONObject(postRequestLogged("complete_task",data));
        return ;
    }

    public JSONObject task_info(String id)throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        JSONObject result = new JSONObject(postRequestLogged("task/"+id,data));
        return result;
    }

    public void uncompleate_task(String id) throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        data.put("id",id);
        JSONObject result = new JSONObject(postRequestLogged("uncomplete_task",data));
        return ;
    }

    public JSONObject add_comment(String id, String comment) throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        data.put("id",id);
        data.put("comment",comment);
        JSONObject result = new JSONObject(postRequestLogged("add_comment",data));
        return result;
    }

    public JSONObject users_and_projects() throws  JSONException{
        HashMap<String,String> data = new HashMap<>();
        String resp = postRequestLogged("users_projects",data);
        JSONObject result = new JSONObject(resp);
        return result;
    }

    public JSONObject add_task(String title, int project_id, int user_id, String desc) throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        data.put("title",title);
        data.put("project_id", String.valueOf(project_id));
        data.put("assigned_id",String.valueOf(user_id));
        data.put("description",desc);
        JSONObject result = new JSONObject(postRequestLogged("add_task",data));
        return result;
    }

    public JSONObject settings(String email, String name, String password) throws  JSONException{
        HashMap<String,String> data = new HashMap<>();
        data.put("name",name);
        data.put("email", email);
        data.put("password",password);
        JSONObject result = new JSONObject(postRequestLogged("settings",data));
        return result;
    }

    public JSONObject projects() throws  JSONException{
        HashMap<String,String> data = new HashMap<>();
        String resp = postRequestLogged("projects",data);
        JSONObject result = new JSONObject(resp);
        return result;
    }

    public JSONObject project(String id) throws  JSONException{
        HashMap<String,String> data = new HashMap<>();
        String resp = postRequestLogged("project/"+id,data);
        JSONObject result = new JSONObject(resp);
        return result;
    }

    public JSONObject project_user(String id) throws  JSONException{
        HashMap<String,String> data = new HashMap<>();
        String resp = postRequestLogged("project_user/"+id,data);
        JSONObject result = new JSONObject(resp);
        return result;
    }

    public JSONObject add_user_to_projec(int id, String project) throws  JSONException{
        HashMap<String,String> data = new HashMap<>();
        data.put("user",String.valueOf(id));
        String resp = postRequestLogged("add_user_to_project/"+project,data);
        JSONObject result = new JSONObject(resp);
        return result;
    }

    public JSONObject add_project(String name) throws JSONException {
        HashMap<String,String> data = new HashMap<>();
        data.put("name",name);
        JSONObject result = new JSONObject(postRequestLogged("add_project",data));
        return result;
    }
}
