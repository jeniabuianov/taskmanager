package jenia.taskmanager.storages;

import android.content.Context;
import android.content.SharedPreferences;

import jenia.taskmanager.interfaces.LocalStorage;

/**
 * Created by jenia on 15.05.18.
 */

public class SharedPref implements LocalStorage {
    private SharedPreferences mSettings;
    private Context context;
    private SharedPreferences.Editor editor;

    public SharedPref(Context context){
        this.context = context;
        mSettings = context.getSharedPreferences("APPO_TaskManager", Context.MODE_PRIVATE);
    }


    @Override
    public boolean empty() {
        return !mSettings.contains("logged");
    }

    @Override
    public boolean has(String key) {
        return mSettings.contains(key);
    }

    @Override
    public String get(String key) {
        return mSettings.getString(key,"");
    }

    @Override
    public void put(String key, String value) {
        mSettings.edit().putString(key,value).apply();
    }

    @Override
    public void remove(String key) {
        mSettings.edit().remove(key).apply();
    }

    @Override
    public void clear(){
        mSettings.edit().clear().apply();
    }
}
