package jenia.taskmanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import jenia.taskmanager.R;

/**
 * Created by jenia on 26.04.18.
 */

public class UsersAdapter extends BaseAdapter {

    private JSONArray list;
    Context ctx;
    LayoutInflater lInflater;

    public UsersAdapter(JSONArray list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
        this.lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.list.length();
    }

    @Override
    public Object getItem(int i) {
        try {
            return this.list.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  0;
    }

    @Override
    public long getItemId(int i) {
        try {
            return this.list.getJSONObject(i).getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = view;
        if (view2 == null) {
            view2 = lInflater.inflate(R.layout.users_list, viewGroup, false);
        }
        TextView user = (TextView) view2.findViewById(R.id.title);
        TextView email = (TextView) view2.findViewById(R.id.email);


        try {
            user.setText(list.getJSONObject(i).getString("name"));
            email.setText(list.getJSONObject(i).getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view2;
    }
}
