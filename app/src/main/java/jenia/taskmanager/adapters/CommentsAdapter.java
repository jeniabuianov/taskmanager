package jenia.taskmanager.adapters;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;
import jenia.taskmanager.fragments.CompletedFragment;
import jenia.taskmanager.fragments.InfoFragment;
import jenia.taskmanager.fragments.MainFragment;

/**
 * Created by jenia on 26.04.18.
 */

public class CommentsAdapter extends BaseAdapter {

    private JSONArray list;
    Context ctx;
    LayoutInflater lInflater;

    public CommentsAdapter(JSONArray list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
        this.lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.list.length();
    }

    @Override
    public Object getItem(int i) {
        try {
            return this.list.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  0;
    }

    @Override
    public long getItemId(int i) {
        try {
            return this.list.getJSONObject(i).getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = view;
        if (view2 == null) {
            view2 = lInflater.inflate(R.layout.comments_list, viewGroup, false);
        }
        TextView user = (TextView) view2.findViewById(R.id.author);
        TextView date = (TextView) view2.findViewById(R.id.date);
        TextView comment = (TextView) view2.findViewById(R.id.comment);


        try {
            user.setText(list.getJSONObject(i).getJSONObject("user").getString("name"));
            date.setText(list.getJSONObject(i).getString("created_at"));
            comment.setText(list.getJSONObject(i).getString("text"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view2;
    }
}
