package jenia.taskmanager.adapters;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;
import jenia.taskmanager.fragments.CompletedFragment;
import jenia.taskmanager.fragments.InfoFragment;
import jenia.taskmanager.fragments.MainFragment;

/**
 * Created by jenia on 26.04.18.
 */

public class TaskAdapter extends BaseAdapter {

    private JSONArray list;
    Context ctx;
    LayoutInflater lInflater;

    public TaskAdapter(JSONArray list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
        this.lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.list.length();
    }

    @Override
    public Object getItem(int i) {
        try {
            return this.list.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  0;
    }

    @Override
    public long getItemId(int i) {
        try {
            return this.list.getJSONObject(i).getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = view;
        if (view2 == null) {
            view2 = lInflater.inflate(R.layout.tasks_list, viewGroup, false);
        }
        TextView title = (TextView) view2.findViewById(R.id.title);
        TextView date = (TextView) view2.findViewById(R.id.date);
        final CheckBox checkBox = (CheckBox) view2.findViewById(R.id.checked);
        final int finali = i;

        try {
            title.setText(list.getJSONObject(i).getString("title"));
            date.setText(list.getJSONObject(i).getString("created_at").substring(0,10));
            if (list.getJSONObject(i).getInt("finished")==1)
                checkBox.setChecked(true);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View view) {

                    try {
                        if (!checkBox.isChecked())
                            CompletedFragment.uncomplete(list.getJSONObject(finali).getInt("id"));
                        else
                            MainFragment.complete(list.getJSONObject(finali).getInt("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText()
                    try {
                        Bundle bundle = new Bundle();
                        bundle.putString("id", list.getJSONObject(finali).getString("id"));
                        InfoFragment fragobj = new InfoFragment();
                        fragobj.setArguments(bundle);
                        MainActivity.setFragment(fragobj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view2;
    }
}
