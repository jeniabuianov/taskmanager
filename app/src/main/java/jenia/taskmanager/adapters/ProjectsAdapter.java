package jenia.taskmanager.adapters;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;
import jenia.taskmanager.fragments.CompletedFragment;
import jenia.taskmanager.fragments.InfoFragment;
import jenia.taskmanager.fragments.InfoProjectFragment;
import jenia.taskmanager.fragments.MainFragment;

/**
 * Created by jenia on 26.04.18.
 */

public class ProjectsAdapter extends BaseAdapter {

    private JSONArray list;
    Context ctx;
    LayoutInflater lInflater;

    public ProjectsAdapter(JSONArray list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
        this.lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.list.length();
    }

    @Override
    public Object getItem(int i) {
        try {
            return this.list.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  0;
    }

    @Override
    public long getItemId(int i) {
        try {
            return this.list.getJSONObject(i).getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = view;
        if (view2 == null) {
            view2 = lInflater.inflate(R.layout.projects_list, viewGroup, false);
        }
        TextView title = (TextView) view2.findViewById(R.id.title);
        TextView date = (TextView) view2.findViewById(R.id.date);
        final int finali = i;

        try {
            title.setText(list.getJSONObject(i).getString("name"));
            date.setText(String.valueOf(list.getJSONObject(i).getJSONArray("users").length())+" users");

            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText()0
                    try {
                        Bundle bundle = new Bundle();
                        bundle.putString("id", list.getJSONObject(finali).getString("id"));
                        InfoProjectFragment fragobj = new InfoProjectFragment();
                        fragobj.setArguments(bundle);
                        MainActivity.setFragment(fragobj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view2;
    }
}
