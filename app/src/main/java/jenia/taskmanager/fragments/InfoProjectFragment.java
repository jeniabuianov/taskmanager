package jenia.taskmanager.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;
import jenia.taskmanager.adapters.CommentsAdapter;
import jenia.taskmanager.adapters.UsersAdapter;

import static jenia.taskmanager.MainActivity.api;

public class InfoProjectFragment extends Fragment {

    public  static Context context;
    private TextView title;
    private TextView project;
    private TextView add_user_text;
    private String id;

    private TextView comment;
    private JSONArray users;
    int user_id = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        id = getArguments().getString("id");
        return inflater.inflate(R.layout.fragment_info_project, container, false);
    }

    public void onStart() {
        super.onStart();
        context = getActivity().getApplicationContext();
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);

        title = (TextView) getActivity().findViewById(R.id.title);
        add_user_text = (TextView) getActivity().findViewById(R.id.a_user);
        project = (TextView) getActivity().findViewById(R.id.project);
        comment = (TextView) getActivity().findViewById(R.id.add_user);
        final Button add_user = (Button) getActivity().findViewById(R.id.add_comm);
        task();

        add_user_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] items = new CharSequence[users.length()+1];
                for(int i=0;i<users.length();i++){
                    try {
                        items[i] = users.getJSONObject(i).getString("name")+" ("+users.getJSONObject(i).getString("email")+")";
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                items[users.length()] = "None";

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select user to add");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item!=users.length()) {
                            try {
                                user_id = users.getJSONObject(item).getInt("id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else user_id = 0;
                        add_user_text.setText(items[item]);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_user();
            }
        });

    }

    private void add_user(){
        if (user_id==0){
            MainActivity.showSnackBar("Please select user to add ", 1000);
            return ;
        }

        Add_User comp = new Add_User(user_id,id);
        comp.execute();

    }

    public void task(){
        Tasks tasks = new Tasks();
        tasks.execute();
    }

    public class Tasks extends AsyncTask<Void, Void, Void> {

        ProgressBar pb;
        public  ListView lvMain;
        RelativeLayout mtp;
        JSONObject task;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb = (ProgressBar)   getActivity().findViewById(R.id.loading);
            lvMain = (ListView) getActivity().findViewById(R.id.grid_list);
            mtp = (RelativeLayout) getActivity().findViewById(R.id.mtp);

            pb.setVisibility(View.VISIBLE);
            mtp.setVisibility(View.INVISIBLE);
            Log.d("Start","tasks started");
        }

        @Override
        protected Void doInBackground(Void... params){

            try {
                task = api.project(id);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                users = api.project_user(id).getJSONArray("users");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            UsersAdapter boxAdapter = null;
            try {
                boxAdapter = new UsersAdapter(task.getJSONObject("project").getJSONArray("users"), getActivity().getBaseContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final ListView lvMain = (ListView) getActivity().findViewById(R.id.grid_list);
            final UsersAdapter finalBoxAdapter = boxAdapter;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    lvMain.setAdapter(finalBoxAdapter);
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.INVISIBLE);
            mtp.setVisibility(View.VISIBLE);
            try {
                title.setText(task.getJSONObject("project").getString("name"));
                TextView link = (TextView) getActivity().findViewById(R.id.link);
                link.setText(task.getString("link"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tasks","fnished");
        }
    }

    public class Add_User extends AsyncTask<Void, Void, Void> {


        int id;
        String project;

        Add_User(int user,String project){
            this.id = user;
            this.project = project;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Start","add_comment");
        }

        @Override
        protected Void doInBackground(Void... params) {

            UsersAdapter boxAdapter = null;
            try {
                JSONObject resp =  api.add_user_to_projec(id,project);
                if (resp.has("users")){
                    boxAdapter = new UsersAdapter(resp.getJSONArray("users"), getActivity().getBaseContext());
                    final ListView lvMain = (ListView) getActivity().findViewById(R.id.grid_list);
                    final UsersAdapter finalBoxAdapter = boxAdapter;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lvMain.setAdapter(finalBoxAdapter);
                        }
                    });
                }
                MainActivity.showSnackBar(resp.getString("message"),2000);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            user_id = 0;
            add_user_text.setText("Click me to add user");
            Log.d("add_comment","fnished");
        }
    }
}
