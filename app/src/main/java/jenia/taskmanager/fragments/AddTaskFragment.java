package jenia.taskmanager.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;

import static jenia.taskmanager.MainActivity.api;

public class AddTaskFragment extends Fragment {

    public  static Context context;
    private TextView project;
    private TextView user;
    private EditText description;
    private String title_;
    private static JSONArray projects;
    private static JSONArray users;
    private int project_id = 0;
    private int user_id = 0;
    private String description_text = "";
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_task, container, false);
    }

    public void onStart() {
        super.onStart();
        context = getActivity().getApplicationContext();
        task();

        Button saveButton = (Button) getActivity().findViewById(R.id.add);
        final EditText title = (EditText) getActivity().findViewById(R.id.title);
        project = (TextView) getActivity().findViewById(R.id.project);
        user = (TextView) getActivity().findViewById(R.id.user);
        description = (EditText) getActivity().findViewById(R.id.description);

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);

        project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = new CharSequence[projects.length()+1];
                for(int i=0;i<projects.length();i++){
                    try {
                        items[i] = projects.getJSONObject(i).getString("name");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                items[projects.length()] = "None";

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Project");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item!=projects.length()) {
                            try {
                                project_id = projects.getJSONObject(item).getInt("id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else project_id = 0;
                        project.setText(items[item]);
                        load_project(project_id);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = new CharSequence[users.length()+1];

                for(int i=0;i<users.length();i++){
                    try {
                        Log.e("NAME",users.getJSONObject(i).getString("name"));
                        items[i] = users.getJSONObject(i).getString("name");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                items[users.length()] = "None";


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Assign task to user");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item!=users.length()) {
                            try {
                                user_id = users.getJSONObject(item).getInt("id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else user_id = 0;
                        user.setText(items[item]);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    title_ = title.getText().toString();
                    description_text = description.getText().toString();
                    AddTask addTask = new AddTask();
                    addTask.execute();
                }
            });
    }

    public void task(){
        Tasks tasks = new Tasks();
        tasks.execute();
    }

    public void load_project(int id){
        Load_Project tasks = new Load_Project(String.valueOf(id));
        tasks.execute();
    }

    public class Load_Project extends AsyncTask<Void, Void, Void> {

        ProgressBar pb;
        RelativeLayout mtp;
        String project_;

        Load_Project(String project){
            this.project_ = project;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb = (ProgressBar)   getActivity().findViewById(R.id.loading);
            mtp = (RelativeLayout) getActivity().findViewById(R.id.mtp);
            pb.setVisibility(View.VISIBLE);
            mtp.setVisibility(View.INVISIBLE);
            Log.d("Start","load started");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONObject project =  api.project(project_);
                users = project.getJSONObject("project").getJSONArray("users");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.INVISIBLE);
            mtp.setVisibility(View.VISIBLE);
        }
    }

    public class AddTask extends AsyncTask<Void, Void, Void> {

        ProgressBar pb;
        RelativeLayout mtp;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb = (ProgressBar)   getActivity().findViewById(R.id.loading);
            mtp = (RelativeLayout) getActivity().findViewById(R.id.mtp);
            pb.setVisibility(View.VISIBLE);
            mtp.setVisibility(View.INVISIBLE);
            Log.d("Start","load started");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONObject response =  api.add_task(title_,project_id,user_id,description_text);
                if (response.has("errors")){
                    MainActivity.showSnackBar(response.getString("errors"),5000);
                }
                else{
                    MainActivity.showSnackBar("Created",5000);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.INVISIBLE);
            mtp.setVisibility(View.VISIBLE);
        }
    }


    public class Tasks extends AsyncTask<Void, Void, Void> {

        ProgressBar pb;
        RelativeLayout mtp;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb = (ProgressBar)   getActivity().findViewById(R.id.loading);
            mtp = (RelativeLayout) getActivity().findViewById(R.id.mtp);
            pb.setVisibility(View.VISIBLE);
            mtp.setVisibility(View.INVISIBLE);
            Log.d("Start","load started");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONObject projects_and_users =  api.users_and_projects();
                projects = projects_and_users.getJSONArray("projects");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.INVISIBLE);
            mtp.setVisibility(View.VISIBLE);
        }
    }
}
