package jenia.taskmanager.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;

import static jenia.taskmanager.MainActivity.api;

public class AddProjectFragment extends Fragment {

    public  static Context context;
    private String title_;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_project, container, false);
    }

    public void onStart() {
        super.onStart();
        context = getActivity().getApplicationContext();
        Button saveButton = (Button) getActivity().findViewById(R.id.add);
        final EditText title = (EditText) getActivity().findViewById(R.id.title);

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);


        saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    title_ = title.getText().toString();
                    AddProject addTask = new AddProject();
                    addTask.execute();
                }
            });
    }

    public class AddProject extends AsyncTask<Void, Void, Void> {

        ProgressBar pb;
        RelativeLayout mtp;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb = (ProgressBar)   getActivity().findViewById(R.id.loading);
            mtp = (RelativeLayout) getActivity().findViewById(R.id.mtp);
            pb.setVisibility(View.VISIBLE);
            mtp.setVisibility(View.INVISIBLE);
            Log.d("Start","load started");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONObject response =  api.add_project(title_);
                if (response.has("errors")){
                    MainActivity.showSnackBar(response.getString("errors"),5000);
                }
                else{
                    MainActivity.showSnackBar("Created",5000);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.INVISIBLE);
            mtp.setVisibility(View.VISIBLE);
        }
    }
}
