package jenia.taskmanager.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;
import jenia.taskmanager.adapters.CommentsAdapter;

import static jenia.taskmanager.MainActivity.api;

public class InfoFragment extends Fragment {

    public  static Context context;
    private TextView title;
    private TextView user;
    private TextView date;
    private TextView project;
    private TextView description;
    private String id;

    private TextView comment;
    private Button comment_button;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        id = getArguments().getString("id");
        return inflater.inflate(R.layout.fragment_info, container, false);
    }

    public void onStart() {
        super.onStart();
        context = getActivity().getApplicationContext();
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);

        title = (TextView) getActivity().findViewById(R.id.title);
        user = (TextView) getActivity().findViewById(R.id.user);
        date = (TextView) getActivity().findViewById(R.id.date);
        project = (TextView) getActivity().findViewById(R.id.project);
        description = (TextView) getActivity().findViewById(R.id.description);

        comment = (TextView) getActivity().findViewById(R.id.add_user);
        comment_button = (Button) getActivity().findViewById(R.id.add_comm);

        task();

        comment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_comment();
            }
        });
    }

    private void add_comment(){
        if (comment.getText().toString().length()<3){
            MainActivity.showSnackBar("Please enter comment", 1000);
            return ;
        }

        Add_Comment comp = new Add_Comment(comment.getText().toString());
        comp.execute();

    }

    public void task(){
        Tasks tasks = new Tasks();
        tasks.execute();
    }

    public class Tasks extends AsyncTask<Void, Void, Void> {

        ProgressBar pb;
        public  ListView lvMain;
        RelativeLayout mtp;
        JSONObject task;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb = (ProgressBar)   getActivity().findViewById(R.id.loading);
            lvMain = (ListView) getActivity().findViewById(R.id.grid_list);
            mtp = (RelativeLayout) getActivity().findViewById(R.id.mtp);

            pb.setVisibility(View.VISIBLE);
            mtp.setVisibility(View.INVISIBLE);
            Log.d("Start","tasks started");
        }

        @Override
        protected Void doInBackground(Void... params){

            try {
                task = api.task_info(id);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            CommentsAdapter boxAdapter = null;
            try {
                boxAdapter = new CommentsAdapter(task.getJSONObject("task").getJSONArray("comments"), getActivity().getBaseContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final ListView lvMain = (ListView) getActivity().findViewById(R.id.grid_list);
            final CommentsAdapter finalBoxAdapter = boxAdapter;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    lvMain.setAdapter(finalBoxAdapter);
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.INVISIBLE);
            mtp.setVisibility(View.VISIBLE);
            try {
                title.setText(task.getJSONObject("task").getString("title"));
                String desc = task.getJSONObject("task").getString("description");
                if (desc.equals("null"))
                    description.setText("");
                else description.setText(desc);
                project.setText(task.getJSONObject("task").getJSONObject("project").getString("name"));
                int finished = task.getJSONObject("task").getInt("finished");
                if (finished==1){
                    date.setText("Finished at "+task.getJSONObject("task").getString("finished_at"));
                }else date.setText(task.getJSONObject("task").getString("created_at"));
                if (task.getJSONObject("task").getJSONObject("assigned")==null){
                    user.setText("");
                }
                else user.setText(task.getJSONObject("task").getJSONObject("assigned").getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("tasks","fnished");
        }
    }

    public class Add_Comment extends AsyncTask<Void, Void, Void> {

        String text;

        Add_Comment(String text){
            this.text = text;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Start","add_comment");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONObject resp =  api.add_comment(id,text);
                if (resp.has("comments")){
                    CommentsAdapter boxAdapter = null;
                    try {
                        boxAdapter = new CommentsAdapter(resp.getJSONArray("comments"), getActivity().getBaseContext());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final ListView lvMain = (ListView) getActivity().findViewById(R.id.grid_list);
                    final CommentsAdapter finalBoxAdapter = boxAdapter;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lvMain.setAdapter(finalBoxAdapter);
                        }
                    });
                }

                MainActivity.showSnackBar(resp.getString("message"),1500);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        comment.setText("");
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.d("add_comment","fnished");
        }
    }
}
