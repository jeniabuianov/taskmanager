package jenia.taskmanager.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import jenia.taskmanager.LoginActivity;
import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;

import static jenia.taskmanager.MainActivity.storage;

/**
 * Created by jenia on 02.06.18.
 */

public class SettingsFragment extends Fragment {

    Button saveButton;
    HashMap<String,String> data = new HashMap<String,String>();


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings_fragment, container, false);
        return rootView;
    }

    public void onStart() {
        super.onStart();

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        saveButton = (Button) getActivity().findViewById(R.id.save_settings);
        getExistingSettings();
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateSettings()){
                    try {
                        rewriteSettings();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private boolean validateSettings(){
        data.clear();
        EditText email = (EditText) getActivity().findViewById(R.id.email);
        EditText name = (EditText) getActivity().findViewById(R.id.name);

        if (email.getText().toString().trim().length()>0){
            data.put("email", String.valueOf(email.getText().toString().trim()));
        }else {
            MainActivity.showSnackBar("Email field is empty",0);
            return false;
        }

        if (name.getText().toString().trim().length()>0){
            data.put("name",name.getText().toString().trim());
        }else{
            MainActivity.showSnackBar("Empty name",0);
            return  false;
        }

        return true;
    }

    private void getExistingSettings(){
        EditText email = (EditText) getActivity().findViewById(R.id.email);
        EditText name = (EditText) getActivity().findViewById(R.id.name);

        email.setText(storage.get("email"));
        name.setText(storage.get("name"));
    }

    private void rewriteSettings() throws JSONException {
        EditText email = (EditText) getActivity().findViewById(R.id.email);
        EditText password = (EditText) getActivity().findViewById(R.id.password);
        EditText name = (EditText) getActivity().findViewById(R.id.name);

        Comp comp = new Comp(name.getText().toString().trim(),email.getText().toString().trim(),password.getText().toString().trim());
        comp.execute();
    }

    public class Comp extends AsyncTask<Object, Object, Void> {

        String email;
        String password;
        String name;
        JSONObject response;

        Comp(String email, String name, String password){
            this.email = email;
            this.name = name;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            response = new JSONObject();
            Log.d("Start","tasks started");
        }

        @Override
        protected Void doInBackground(Object... params) {

            try {
                response = MainActivity.api.settings(name,email,password);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return  null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (response.has("errors")){
                try {
                    MainActivity.showSnackBar(response.getString("errors"),5000);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return ;
            }
            if (password.toString().trim().length()>0){
                storage.clear();
                startActivity(new Intent(getActivity().getApplicationContext(),LoginActivity.class));
                getActivity().finish();
            }
            else{
                storage.remove("name");
                storage.remove("email");
                storage.put("name",email);
                storage.put("email",name);
                TextView nameLeft = (TextView) getActivity().findViewById(R.id.name_on_left);
                TextView emailLeft = (TextView) getActivity().findViewById(R.id.email_on_left);
                nameLeft.setText(storage.get("name"));
                emailLeft.setText(storage.get("email"));
                MainActivity.showSnackBar("Saved",2000);
                MainActivity.setFragment(new MainFragment());
            }
            Log.d("tasks","fnished");
        }
    }

}
