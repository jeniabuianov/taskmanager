package jenia.taskmanager.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONException;

import jenia.taskmanager.MainActivity;
import jenia.taskmanager.R;
import jenia.taskmanager.adapters.ProjectsAdapter;
import jenia.taskmanager.adapters.TaskAdapter;

import static jenia.taskmanager.MainActivity.api;
import static jenia.taskmanager.MainActivity.setFragment;

public class ProjectsFragment extends Fragment {

    public  static Context context;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_projects, container, false);
    }

    public void onStart() {
        super.onStart();
        context = getActivity().getApplicationContext();
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        FloatingActionButton fab_project = (FloatingActionButton) getActivity().findViewById(R.id.fab_project);
        fab_project.setVisibility(View.VISIBLE);
        fab_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new AddProjectFragment());
            }
        });
        task();
    }

    public void task(){
        Tasks tasks = new Tasks();
        tasks.execute();
    }


    public class Tasks extends AsyncTask<Void, Void, Void> {

        ProgressBar pb;
        ListView lvMain;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb = (ProgressBar)   getActivity().findViewById(R.id.loading);
            lvMain = (ListView) getActivity().findViewById(R.id.grid_list);

            pb.setVisibility(View.VISIBLE);
            lvMain.setVisibility(View.INVISIBLE);
            Log.d("Start","tasks started");
        }

        @Override
        protected Void doInBackground(Void... params) {
            ProjectsAdapter boxAdapter = null;
            try {
                boxAdapter = new ProjectsAdapter(api.projects().getJSONArray("projects"), getActivity().getBaseContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final ListView lvMain = (ListView) getActivity().findViewById(R.id.grid_list);
            final ProjectsAdapter finalBoxAdapter = boxAdapter;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    lvMain.setAdapter(finalBoxAdapter);
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pb.setVisibility(View.INVISIBLE);
            lvMain.setVisibility(View.VISIBLE);
            Log.d("tasks","fnished");
        }
    }
}
