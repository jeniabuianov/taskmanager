package jenia.taskmanager;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import jenia.taskmanager.adapters.TaskAdapter;
import jenia.taskmanager.fragments.AddTaskFragment;
import jenia.taskmanager.fragments.AllTasksFragment;
import jenia.taskmanager.fragments.CompletedFragment;
import jenia.taskmanager.fragments.MainFragment;
import jenia.taskmanager.fragments.ProjectsFragment;
import jenia.taskmanager.fragments.SettingsFragment;
import jenia.taskmanager.interfaces.LocalStorage;
import jenia.taskmanager.storages.API;
import jenia.taskmanager.storages.SharedPref;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static  View parentLayout;
    public static LocalStorage storage;
    public static API api;
    public  static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        parentLayout = findViewById(android.R.id.content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new AddTaskFragment());
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragmentManager = getFragmentManager();
        storage = new SharedPref(getApplicationContext());
        api = new API(getApplicationContext());
        setFragment(new MainFragment());
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                TextView nameLeft = (TextView) findViewById(R.id.name_on_left);
                TextView emailLeft = (TextView) findViewById(R.id.email_on_left);
                nameLeft.setText(storage.get("name"));
                emailLeft.setText(storage.get("email"));
                return;
            }
        },1500);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            setFragment(new SettingsFragment());
        }

        if(id==R.id.action_logout){
            storage.clear();
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (id==R.id.nav_projects){
            setFragment(new ProjectsFragment());
        }

        if (id == R.id.nav_next_tasks){
            setFragment(new MainFragment());
        }

        if (id==R.id.nav_completed){
            setFragment(new CompletedFragment());
        }

        if (id==R.id.nav_all){
            setFragment(new AllTasksFragment());
        }
        return true;
    }

    public static void showSnackBar(String text, int length){
        final Snackbar snackbar = Snackbar.make(parentLayout, text, length);
        snackbar.setAction("X", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        }).show();
    }
    public static void setFragment(Fragment fragment){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.layout_main, fragment, fragment.getClass().toString());
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
