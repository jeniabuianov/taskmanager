package jenia.taskmanager.interfaces;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by jenia on 17.05.18.
 */

public interface RemoteStorage{

    boolean hasConnection();
    String getRequest(String url);
    String postRequest(String url, HashMap<String,String> data);
    String postRequestLogged(String url, HashMap<String,String> data);
}