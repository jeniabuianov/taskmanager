package jenia.taskmanager.interfaces;

import android.content.Context;

/**
 * Created by jenia on 15.05.18.
 */

public interface LocalStorage {

    boolean empty();
    boolean has(String key);
    String get(String key);
    void put(String key, String value);
    void remove(String key);
    void clear();

}
